package com.mine.libs.comm;

import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Crash implements UncaughtExceptionHandler {

    private static String TAG = Comm.getClassName();
    private static Crash mCrash = null;

    public static void setCrash(){
        if(mCrash == null){
            synchronized (Crash.class){
                mCrash = new Crash();
                Thread.setDefaultUncaughtExceptionHandler(mCrash);
            }
        }
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        if(handleException(thread, throwable)){
            System.exit(1);
        }else {
            UncaughtExceptionHandler uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if(uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, throwable);
            }
        }
    }

    private boolean handleException(Thread thread, Throwable throwable) {
        String crashInfo = Comm.mEnterChar + "Crash:" + Comm.mEnterChar + Comm.getStackTraceString(throwable) + Comm.mEnterChar;
        /**Output log*/
        Debug.e(TAG, crashInfo);
        /**Save file*/
        saveFile(crashInfo);
        /**Do something*/
        if(mCrashHandler !=null) {
            mCrashHandler.onCrash(thread, throwable, crashInfo);
        }
        return true;
    }

    private boolean saveFile(String data) {
        String javaName = Comm.getJava();
        String filePath = Comm.mSlashChar + "z" + javaName + Comm.mSlashChar + "Crash" + Comm.mSlashChar;
        String fileName = javaName + ".txt";
        SimpleDateFormat simpleDateFormat = null;
        String time = null;
        String prefix = null;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        time = simpleDateFormat.format(new Date());
        prefix ="crash-" + time + "-";
        return Comm.saveFile(Comm.mHomeRoot + filePath, prefix + fileName, false, data.getBytes());
    }

    public interface CrashHandler{
        void onCrash(Thread thread, Throwable throwable, String crashInfo);
    }
    private static CrashHandler mCrashHandler = null;

    public static void setCrashHandler(CrashHandler crashHandler){
        mCrashHandler = crashHandler;
    }
}