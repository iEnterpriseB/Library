package com.mine.libs.comm;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.UnknownHostException;

public class Comm{

    /************************************************************************
     * Constants and variables define
     ***********************************************************************/
    /** Common constants*/
    private static String               JAVA_NAME            = "Mine";

    /** Common variables*/

    public static Sysdef                mSystem             = Sysdef.SYSTEM_OTHERS;

    public static String                mHomeRoot           = "";

    public static String                mSlashChar          = "";
    
    public static String                mEnterChar          = "";

    /************************************************************************
     * Functions define
     ***********************************************************************/
    /** System define*/
    public enum Sysdef {
        SYSTEM_WINDOWS,
        SYSTEM_LINUX,
        SYSTEM_OTHERS
    }

    public static void setComm() {
        String osname = System.getProperty("os.name");
        String userhome = System.getProperty("user.home");
        if(osname == null) {
            mSystem = Sysdef.SYSTEM_OTHERS;
            mHomeRoot = "";
            mSlashChar = "";
            mEnterChar = "";
        } else if(osname.toLowerCase().indexOf("windows") >= 0) {
            mSystem = Sysdef.SYSTEM_WINDOWS;
            mHomeRoot = userhome + "\\Desktop";
            mSlashChar = "\\";
            mEnterChar = "\r\n";
        } else if(osname.toLowerCase().indexOf("linux") >= 0) {
            mSystem = Sysdef.SYSTEM_LINUX;
            mHomeRoot = userhome + "/Desktop";
            mSlashChar = "/";
            mEnterChar = "\n";
        } else {
            mSystem = Sysdef.SYSTEM_OTHERS;
            mHomeRoot = "";
            mSlashChar = "";
            mEnterChar = "";
        }
    }

    public static void setSystem(Sysdef system) {
        mSystem = system;
    }

    public static Sysdef getSystem() {
        return mSystem;
    }

    public static void setJava(String javaName) {
        JAVA_NAME = javaName;
    }

    public static String getJava() {
        return JAVA_NAME;
    }

    /************************************************************************
     * Comm functions define
     ***********************************************************************/
    /**
     * Get current process name
     * @return String
     *         Current process name
     */
    public static String getProcessName() {
        String processName = null;
        try {
            processName = ManagementFactory.getRuntimeMXBean().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processName;
    }

    /**
     * Get current process PID
     * @return long
     *         Current process PID
     */
    public static long getProcessID() {
        long processID = 0;
        try {
            String name = ManagementFactory.getRuntimeMXBean().getName();
            name = name.substring(0, name.indexOf('@'));
            processID = Long.parseLong(name);
            /*RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
            Field field = runtimeMXBean.getClass().getDeclaredField("jvm");
            field.setAccessible(true);
            VMManagement vMManagement = (VMManagement) field.get(runtimeMXBean);
            Method method = vMManagement.getClass().getDeclaredMethod("getProcessId");
            method.setAccessible(true);
            pid = Long.parseLong(method.invoke(vMManagement).toString());*/
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  processID;
    }

    /**
     * Get current thread name
     * @return String
     *         Current thread name
     */
    public static String getThreadName() {
        String threadName = null;
        try {
            threadName = Thread.currentThread().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return threadName;
    }

    /**
     * Get current thread TID
     * @return long
     *         Current thread TID
     */
    public static long getThreadID() {
        long threadID = 0;
        try {
            threadID = Thread.currentThread().getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  threadID;
    }

    /** Stack deep*/
    private static final int stackdeep = 1;

    /**
     * Get file name
     * @return String
     *         file name
     */
    public static String getFileName() {
        return new Throwable().getStackTrace()[stackdeep].getFileName();
    }

    /**
     * Get function name
     * @return String
     *         function name
     */
    public static String getFuncName() {
        return new Throwable().getStackTrace()[stackdeep].getMethodName();
    }

    /**
     * Get line number
     * @return int
     *         line number
     */
    public static int getLineNumber() {
        return new Throwable().getStackTrace()[stackdeep].getLineNumber();
    }

    /**
     * Get package name and class name
     * @return String
     *         package name and class name
     */
    public static String getPkgClass() {
        return new Throwable().getStackTrace()[stackdeep].getClassName();
    }

    /**
     * Get package name
     * @return String
     *         package name
     */
    public static String getPkgName() {
        String string = new Throwable().getStackTrace()[stackdeep].getClassName();
        string = string.substring(0, string.lastIndexOf('.'));
        return string;
    }

    /**
     * Get class name
     * @return String
     *         class name
     */
    public static String getClassName() {
        String string = new Throwable().getStackTrace()[stackdeep].getClassName();
        string = string.substring(string.lastIndexOf('.') + 1);
        return string;
    }

    /**
     * Save buffer to file
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @param append
     *         append
     * @param buffer
     *         buffer
     * @return boolean
     *
     */
    public static boolean saveFile(String filePath, String fileName, boolean append, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            if(!filePath.endsWith(mSlashChar)){
                filePath = filePath + mSlashChar;
            }
            File path = new File(filePath);
            if (!path.exists()) {
                if(!path.mkdirs()){
                    return false;
                }
            }
            File file = new File(filePath + fileName);
            if (!file.exists()) {
                if(!file.createNewFile()) {
                    return false;
                }
            }
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(buffer);
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Save buffer to file
     * @param filePath
     *         the file path with file name
     * @param append
     *         append
     * @param buffer
     *         buffer
     * @return boolean
     *
     */
    public static boolean saveFile(String filePath, boolean append, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            String fileDirs = filePath.substring(0, filePath.lastIndexOf(Comm.mSlashChar));
            File path = new File(fileDirs);
            if (!path.exists()) {
                if(!path.mkdirs()){
                    return false;
                }
            }
            File file = new File(filePath);
            if (!file.exists()) {
                if(!file.createNewFile()) {
                    return false;
                }
            }
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(buffer);
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @return byte[]
     *
     */
    public static byte[] readFile(String filePath, String fileName) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        try {
            if(!filePath.endsWith(mSlashChar)){
                filePath = filePath + mSlashChar;
            }
            File file = new File(filePath + fileName);
            byte[] buffer = new byte[((Long)file.length()).intValue()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return buffer;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path with file name
     * @return byte[]
     *
     */
    public static byte[] readFile(String filePath) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        try {
            File file = new File(filePath);
            byte[] buffer = new byte[((Long)file.length()).intValue()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return buffer;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @param buffer
     *         the buffer of the file
     * @return boolean
     *
     */
    public static boolean readFile(String filePath, String fileName, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            if(!filePath.endsWith(mSlashChar)){
                filePath = filePath + mSlashChar;
            }
            File file = new File(filePath + fileName);
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path with file name
     * @param buffer
     *         the buffer of the file
     * @return boolean
     *
     */
    public static boolean readFile(String filePath, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete file
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @return boolean
     *
     */
    public static boolean deleteFile(String filePath, String fileName) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        try {
            if(!filePath.endsWith(mSlashChar)){
                filePath = filePath + mSlashChar;
            }
            File file = new File(filePath + fileName);
            if (file.exists() && file.isFile()) {
                return file.delete();
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete to file
     * @param filePath
     *         the file path with file name
     * @return boolean
     *
     */
    public static boolean deleteFile(String filePath) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        try {
            File file = new File(filePath);
            if (file.exists() && file.isFile()) {
                return file.delete();
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Copy file
     * @param fromFilePath
     *         the file path(from) with file name
     * @param toFilePath
     *         the file path(to) with file name
     * @return boolean
     *
     */
    public static boolean copyFile(String fromFilePath, String toFilePath) {
        if (fromFilePath == null) {
            throw new NullPointerException("fromFilePath null");
        }
        if (toFilePath == null) {
            throw new NullPointerException("toFilePath null");
        }
        try {
            FileInputStream fisFrom = new FileInputStream(fromFilePath);
            FileOutputStream fosTo = new FileOutputStream(toFilePath);
            byte b[] = new byte[1024];
            int n;
            while ((n = fisFrom.read(b)) > 0) {
                fosTo.write(b, 0, n);
            }
            fisFrom.close();
            fosTo.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Rename file
     * @param oldFilePath
     *         the file path(old) with file name
     * @param newFilePath
     *         the file path(new) with file name
     * @return boolean
     *
     */
    public static boolean renameFile(String oldFilePath, String newFilePath) {
        if (oldFilePath == null) {
            throw new NullPointerException("oldFilePath null");
        }
        if (newFilePath == null) {
            throw new NullPointerException("newFilePath null");
        }
        try {
            File oldFile = new File(oldFilePath);
            File newFile = new File(newFilePath);
            return oldFile.renameTo(newFile);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get stack trace string
     * @param tr
     *         Throwable
     * @return String
     *
     */
    public static String getStackTraceString(Throwable tr) {
        if (tr == null) {
            return "";
        }
        // This is to reduce the amount of log spew that apps do in the non-error
        // condition of the network being unavailable.
        Throwable t = tr;
        while (t != null) {
            if (t instanceof UnknownHostException) {
                return "";
            }
            t = t.getCause();
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    /************************************************************************
     * Comm utils define
     ***********************************************************************/
    /**
     * Convert byte to hex string.
     * @param src
     *         byte data
     * @return
     *  hex string
     */
    public static String byteToHexString(byte src) {
        int v = src & 0xFF;
        String hz = "";
        String hv = Integer.toHexString(v);
        if (hv.length() < 2) {
            hz = "0";
        }
        hv = hz + hv;
        return hv;
    }

    /**
     * Convert byte[] to hex string.
     * @param src
     *         byte[] data
     * @return
     *  hex string
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * Parse byte[] to hex string.
     * @param src
     *         byte[] data
     * @return
     *  hex string
     */
    public static String parseBytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            stringBuilder.append("0x");
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
            if(i != src.length-1) {
                stringBuilder.append(" ");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Convert hex string to byte
     * @param hexString
     *         the hex string
     * @return byte
     */
    public static byte hexStringToByte(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return -1;
        }
        byte d = (byte) 0x00;
        int length = hexString.length();
        if(length == 1){
            hexString = "0" + hexString;
        }
        if(length > 2){
            hexString = hexString.substring(length-2, length);
        }
        d = (byte)(charToByte(hexString.charAt(0)) << 4 | charToByte(hexString.charAt(1)));
        return d;
    }

    /**
     * Convert hex string to byte[]
     * @param hexString
     *         the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Parse hex string to byte[]
     * @param hexString
     *         the hex string
     * @return byte[]
     */
    public static byte[] parseHexStringToBytes(String hexString) {
        boolean flag = false;
        int length = 0;
        int i = 0, j = 0;
        int start = 0, end = 0;
        byte[] buffer1 = null, buffer2 = null;
        length = hexString.length();
        buffer1 = new byte[length/2+1];
        for(i = 0; i < length; i++) {
            char ch = hexString.charAt(i);
            if(flag == false) {
                if((ch ==' ') || (ch =='\n') || (ch =='\r') || (ch =='\t')) {
                    flag = true;
                    end = i;
                    if(start<end) {
                        buffer1[j++] = hexStringToByte(hexString.substring(start, end));
                    }
                }
            }
            if(flag == true) {
                if(!((ch ==' ') || (ch =='\n') || (ch =='\r') || (ch =='\t'))) {
                    flag = false;
                    start = i;
                }
            }
            if(i == length-1) {
                if(!((ch ==' ') || (ch =='\n') || (ch =='\r') || (ch =='\t'))) {
                    flag = true;
                    end = length;
                    if(start<end) {
                        buffer1[j++] = hexStringToByte(hexString.substring(start, end));
                    }
                }
            }
        }
        length = j--;
        buffer2 = new byte[length];
        for(i = 0; i < length; i++) {
            buffer2[i] = buffer1[i];
        }
        return buffer2;
    }

    /** 
     * Convert char to byte 
     * @param c 
     *         char 
     * @return byte 
     *         
     */  
     public static byte charToByte(char c) {
         byte B = (byte) "0123456789ABCDEF".indexOf(c);
         if(B != -1) {
             return B;
         }
         return (byte) "0123456789abcdef".indexOf(c);
    }
}
