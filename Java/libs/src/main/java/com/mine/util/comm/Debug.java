package com.mine.libs.comm;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

public class Debug {

    /************************************************************************
     * Constants define
     ***********************************************************************/
    public static final String          ALL_TAG             = "Mine";

    public static final boolean         ALL_SWT             = true;

    public static final boolean         LOG_SWT             = true;

    public static final boolean         FLE_SWT             = false;

    public static final boolean         EXT_SWT             = true;

    /************************************************************************
     * Functions define
     ***********************************************************************/
    public static void setDebug() {
        mPriority = ALL;
    }

    public static void endDebug() {
        if(mQueue != null) {
            synchronized (mQueue) {
                mRun = false;
                mQueue.notify();
            }
        }
        if(mIsNewTrd && (mThread != null)) {
            try {
                mThread.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void v(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxV(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("V", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void v(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxV(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("V", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void d(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxD(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("D", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void d(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxD(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("D", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void i(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxI(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("I", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void i(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxI(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("I", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void w(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxW(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("W", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void w(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxW(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("W", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void e(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxE(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("E", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void e(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                LogxE(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("E", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }

    /**
     * Get FILE, FUNC, LINE and CLASS.
     * @return String or int
     *         result of FILE, FUNC, LINE and CLASS
     */
    private static final int stackdeep = 1;

    public static String FILE() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getFileName();
            }
        }
        return "";
    }

    public static String FUNC() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getMethodName();
            }
        }
        return "";
    }

    public static int LINE() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getLineNumber();
            }
        }
        return 0;
    }

    public static String CLASS() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getClassName();
            }
        }
        return "";
    }

    public static String CLASSPKG() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                String string = new Throwable().getStackTrace()[stackdeep].getClassName();
                string = string.substring(0, string.lastIndexOf('.'));
                return string;
            }
        }
        return "";
    }

    public static String CLASSSMP() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                String string = new Throwable().getStackTrace()[stackdeep].getClassName();
                string = string.substring(string.lastIndexOf('.') + 1);
                return string;
            }
        }
        return "";
    }

    /**
     * Priority constant for the println method.
     */
    public static final int ALL = 1;

    /**
     * Priority constant for the println method; use Log.v.
     */
    public static final int VERBOSE = 2;

    /**
     * Priority constant for the println method; use Log.d.
     */
    public static final int DEBUG = 3;

    /**
     * Priority constant for the println method; use Log.i.
     */
    public static final int INFO = 4;

    /**
     * Priority constant for the println method; use Log.w.
     */
    public static final int WARN = 5;

    /**
     * Priority constant for the println method; use Log.e.
     */
    public static final int ERROR = 6;

    /**
     * Priority constant for the println method.
     */
    public static final int ASSERT = 7;

    /**
     * Priority variable for the println method.
     */
    private static int mPriority = ASSERT;


    public static void LogxV(String tag, String msg){
        if(mPriority <= VERBOSE) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " V/" + tag + ": " + msg);
        }
    }
    
    public static void LogxV(String tag, String msg, Throwable tr){
        if(mPriority <= VERBOSE) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " V/" + tag + ": " + msg + '\n' + getStackTraceString(tr));
        }
    }
    
    public static void LogxD(String tag, String msg){
        if(mPriority <= DEBUG) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " D/" + tag + ": " + msg);
        }
    }
    
    public static void LogxD(String tag, String msg, Throwable tr){
        if(mPriority <= DEBUG) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " D/" + tag + ": " + msg + '\n' + getStackTraceString(tr));
        }
    }
    
    public static void LogxI(String tag, String msg){
        if(mPriority <= INFO) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " I/" + tag + ": " + msg);
        }
    }
    
    public static void LogxI(String tag, String msg, Throwable tr){
        if(mPriority <= INFO) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " I/" + tag + ": " + msg + '\n' + getStackTraceString(tr));
        }
    }
    
    public static void LogxW(String tag, String msg){
        if(mPriority <= WARN) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " W/" + tag + ": " + msg);
        }
    }
    
    public static void LogxW(String tag, String msg, Throwable tr){
        if(mPriority <= WARN) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " W/" + tag + ": " + msg + '\n' + getStackTraceString(tr));
        }
    }
    
    public static void LogxE(String tag, String msg){
        if(mPriority <= ERROR) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " E/" + tag + ": " + msg);
        }
    }
    
    public static void LogxE(String tag, String msg, Throwable tr){
        if(mPriority <= ERROR) {
            System.out.println(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " E/" + tag + ": " + msg + '\n' + getStackTraceString(tr));
        }
    }

    public static void LogxF(String lvl, String tag, String msg){
        saveLogfile(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " " + lvl + "/" + tag + ": " + msg);
    }

    public static void LogxF(String lvl, String tag, String msg, Throwable tr){
        saveLogfile(Comm.getProcessID() + "-"+ Comm.getThreadID()  + " " + lvl + "/" + tag + ": " + msg + "\n" + getStackTraceString(tr));
    }

    private static String getStackTraceString(Throwable tr) {
        if (tr == null) {
            return "";
        }
        // This is to reduce the amount of log spew that apps do in the non-error
        // condition of the network being unavailable.
        Throwable t = tr;
        while (t != null) {
            if (t instanceof UnknownHostException) {
                return "";
            }
            t = t.getCause();
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    /*
    private static HandlerThread        mThread         = null;
    private static Handler              mHandler        = null;
    private static final boolean        mIsNewTrd       = true;

    private static void saveLogfile(String msg) {
        if(mIsNewTrd) {
            if ((mThread == null) || (mHandler == null)) {
                mThread = new HandlerThread("mThread");
                mThread.start();
                mHandler = new Handler(mThread.getLooper()) {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        saveFile(msg.obj.toString());
                    }
                };
            }
            Message message = Message.obtain();
            message.obj = msg;
            mHandler.sendMessage(message);
        }else {
            saveFile(msg);
        }
    }
    */
    private static Thread               mThread         = null;
    private static Queue<String>        mQueue          = null;
    private static boolean              mRun            = true;
    private static final boolean        mIsNewTrd       = true;

    private static void saveLogfile(String msg) {
        if(mIsNewTrd) {
            if((mThread == null) || (mQueue == null)) {
                mQueue = new LinkedList<String>();
                mThread = new Thread() {
                    @Override
                    public void run() {
                        while (mRun) {
                            if(mQueue != null) {
                                synchronized (mQueue) {
                                    if(mQueue.size() <= 0) {
                                        try {
                                            mQueue.wait();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    while(mQueue.size() > 0) {
                                        saveFile(mQueue.poll());
                                    }
                                }
                            }
                        }
                    }
                };
                mThread.start();
            }
            if(mQueue != null) {
                synchronized (mQueue) {
                    mQueue.offer(msg);
                    mQueue.notify();
                }
            }
        } else {
            saveFile(msg);
        }
    }

    private static boolean saveFile(String data) {
        String javaName = Comm.getJava();
        String filePath = Comm.mSlashChar + "z" + javaName + Comm.mSlashChar + "Log" + Comm.mSlashChar;
        String fileName = javaName + ".txt";
        SimpleDateFormat simpleDateFormat = null;
        String time = null;
        String prefix = null;
        String contents = null;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH");
        time = simpleDateFormat.format(new Date());
        prefix = "log-" + time + "-";
        simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
        time = simpleDateFormat.format(new Date());
        contents = time + '\t' + data + Comm.mEnterChar;
        return Comm.saveFile(Comm.mHomeRoot + filePath, prefix + fileName, true, contents.getBytes());
    }
}
