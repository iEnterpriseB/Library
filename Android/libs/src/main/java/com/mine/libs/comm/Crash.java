package com.mine.libs.comm;

import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Crash implements UncaughtExceptionHandler {

    private static String TAG = Comm.getClassName();
    private static Context mContext = null;
    private static Crash mCrash = null;

    static {
        if(mCrash == null){
            synchronized (Crash.class){
                mCrash = new Crash();
                Thread.setDefaultUncaughtExceptionHandler(mCrash);
            }
        }
    }

    public static void setContext(Context context) {
        if(context != null) {
            mContext = context.getApplicationContext();
        }
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        if(handleException(thread, throwable)){
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }else {
            UncaughtExceptionHandler uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if(uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, throwable);
            }
        }
    }

    private boolean handleException(Thread thread, Throwable throwable) {
        StringBuffer bldInfoBuffer = new StringBuffer();
        try {
            Field[] fields = Build.class.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String key = field.getName();
                String value = field.get(null).toString();
                String result = key + " = " + value;
                bldInfoBuffer.append(result).append("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String crashInfo = "\r\nCrash:\r\n" +
            bldInfoBuffer.toString() + "\r\n" +
            Comm.getStackTraceString(throwable) + "\r\n";

        /**Output log*/
        Debug.e(TAG, crashInfo);

        /**Save file*/
        saveFile(crashInfo);

        /**Do something*/
        if(mCrashHandler !=null) {
            mCrashHandler.onCrash(thread, throwable, crashInfo);
        }

        /**Popup toast*/
        if(mContext != null) {
            new Thread() {
                @Override
                public void run() {
                    Looper.prepare();
                    try {
                        String appName = Comm.getApp();
                        Toast.makeText(mContext, appName + " Error !!!", Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Looper.loop();
                }
            }.start();
        }
        try {
            Thread.sleep(4000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private boolean saveFile(String data) {
        String appName = Comm.getApp();
        String filePath = "/z" + appName + "/Crash/";
        String fileName = appName + ".txt";
        SimpleDateFormat simpleDateFormat = null;
        String time = null;
        String prefix = null;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        time = simpleDateFormat.format(new Date());
        prefix ="crash-" + "-" + time + "-";
        String storage = Comm.getStorage();
        if(storage != null) {
            return Comm.saveFile(storage + filePath, prefix + fileName, false, data.getBytes());
        } else {
            return false;
        }
    }

    public interface CrashHandler{
        void onCrash(Thread thread, Throwable throwable, String crashInfo);
    }
    private static CrashHandler mCrashHandler = null;

    public static void setCrashHandler(CrashHandler crashHandler){
        mCrashHandler = crashHandler;
    }
}