package com.mine.libs.comm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Set;

public class Comm{

    private static Context              mContext            = null;

    /************************************************************************
     * Variables define
     ***********************************************************************/
    private static String               APP_NAME            = "Unknown";

    /************************************************************************
     * Functions define
     ***********************************************************************/
    static {
    }

    public static void init(Context context, String appName) {
        Comm.setContext(context);
        Crash.setContext(context);
        Debug.setContext(context);
        Comm.setApp(appName);
        Debug.setTag(appName);
    }

    public static void setContext(Context context) {
        if(context != null) {
            mContext = context.getApplicationContext();
        }
    }

    public static Context getContext() {
        return mContext;
    }

    public static void setApp(String appName) {
        APP_NAME = appName;
    }

    public static String getApp() {
        return APP_NAME;
    }

    /************************************************************************
     * Comm functions define
     ***********************************************************************/
    /** app information*/
    public static class AppInfo {
        public String appName;
        public String appPkgName;
        public String appVerName;
        public int appVerCode;
    }

    /**
     * Get app information
     * @param context
     *         the context
     * @return AppInfo
     *         app information
     */
    public static AppInfo getAppInfo(Context context) {
        AppInfo appInfo = new AppInfo();
        appInfo.appName = null;
        appInfo.appPkgName = null;
        appInfo.appVerName = null;
        appInfo.appVerCode  = 0x00;
        try {
            if(context != null) {
                PackageManager packageManager = context.getPackageManager();
                PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(),0x00);
                if (packageInfo != null) {
                    appInfo.appName = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                    appInfo.appPkgName = packageInfo.packageName;
                    appInfo.appVerName = packageInfo.versionName;
                    appInfo.appVerCode  = packageInfo.versionCode;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return appInfo;
    }

    /**
     * Get app name
     * @param context
     *         the context
     * @return String
     *         App name
     */
    public static String getAppName(Context context) {
        String appName = null;
        PackageManager packageManager = null;
        PackageInfo packageInfo = null;
        String packageName = null;
        try {
            if(context != null) {
                packageManager = context.getPackageManager();
                packageName = context.getPackageName();
                packageInfo = packageManager.getPackageInfo(packageName,0x00);
                if (packageInfo != null) {
                    appName = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return appName;
    }

    /**
     * Get app package name
     * @param context
     *         the context
     * @return String
     *         App package name
     */
    public static String getAppPkgName(Context context) {
        String pkgName = null;
        try {
            if (context != null) {
                pkgName = context.getPackageName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pkgName;
    }

    /**
     * Get app version name
     * @param context
     *         the context
     * @return String
     *         App version name
     */
    public static String getAppVerName(Context context) {
        String verName = null;
        PackageManager packageManager = null;
        PackageInfo packageInfo = null;
        String packageName = null;
        try {
            if(context != null) {
                packageManager = context.getPackageManager();
                packageName = context.getPackageName();
                packageInfo = packageManager.getPackageInfo(packageName,0x00);
                if (packageInfo != null) {
                    verName = packageInfo.versionName;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return verName;
    }

    /**
     * Get app version code
     * @param context
     *         the context
     * @return int
     *         App version code
     */
    public static int getAppVerCode(Context context) {
        int verCode = 0x00;
        PackageManager packageManager = null;
        PackageInfo packageInfo = null;
        String packageName = null;
        try {
            if(context != null) {
                packageManager = context.getPackageManager();
                packageName = context.getPackageName();
                packageInfo = packageManager.getPackageInfo(packageName,0x00);
                if (packageInfo != null) {
                    verCode = packageInfo.versionCode;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return verCode;
    }

    /**
     * Get current process name
     * @return String
     *         Current process name
     */
    public static String getProcessName() {
        String processName = null;
        try {
            File file = new File("/proc/" + android.os.Process.myPid() + "/" + "cmdline");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            processName = reader.readLine().trim();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processName;
    }

    /**
     * Get current process PID
     * @return long
     *         Current process PID
     */
    public static long getProcessID() {
        long processID = 0;
        try {
            processID = android.os.Process.myPid();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  processID;
    }

    /**
     * Get current thread name
     * @return String
     *         Current thread name
     */
    public static String getThreadName() {
        String threadName = null;
        try {
            threadName = Thread.currentThread().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return threadName;
    }

    /**
     * Get current thread TID
     * @return long
     *         Current thread TID
     */
    public static long getThreadID() {
        long threadID = 0;
        try {
            threadID = Thread.currentThread().getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  threadID;
    }

    /**
     * Get storage path
     * @return String
     *
     */
    public static String getStorage() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return Environment.getExternalStorageDirectory().getPath();
        } else {
            return null;
        }
    }

    /** Stack deep*/
    private static final int stackdeep = 1;

    /**
     * Get file name
     * @return String
     *         file name
     */
    public static String getFileName() {
        return new Throwable().getStackTrace()[stackdeep].getFileName();
    }

    /**
     * Get function name
     * @return String
     *         function name
     */
    public static String getFuncName() {
        return new Throwable().getStackTrace()[stackdeep].getMethodName();
    }

    /**
     * Get line number
     * @return int
     *         line number
     */
    public static int getLineNumber() {
        return new Throwable().getStackTrace()[stackdeep].getLineNumber();
    }

    /**
     * Get package name and class name
     * @return String
     *         package name and class name
     */
    public static String getPkgClass() {
        return new Throwable().getStackTrace()[stackdeep].getClassName();
    }

    /**
     * Get package name
     * @return String
     *         package name
     */
    public static String getPkgName() {
        String string = new Throwable().getStackTrace()[stackdeep].getClassName();
        string = string.substring(0, string.lastIndexOf('.'));
        return string;
    }

    /**
     * Get class name
     * @return String
     *         class name
     */
    public static String getClassName() {
        String string = new Throwable().getStackTrace()[stackdeep].getClassName();
        string = string.substring(string.lastIndexOf('.') + 1);
        return string;
    }

    /**
     * Is file exist
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @return boolean
     *
     */
    public static boolean isFileExist(String filePath, String fileName) {
        if (filePath == null) {
            return false;
        }
        if (fileName == null) {
            return false;
        }
        if(!filePath.endsWith("/")){
            filePath = filePath + "/";
        }
        File file = new File(filePath + fileName);
        if (!file.exists()) {
            return false;
        }
        return true;
    }

    /**
     * Is file exist
     * @param filePath
     *         the file path with file name
     * @return boolean
     *
     */
    public static boolean isFileExist(String filePath) {
        if (filePath == null) {
            return false;
        }
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }
        return true;
    }

    /**
     * Save buffer to file
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @param append
     *         append
     * @param buffer
     *         buffer
     * @return boolean
     *
     */
    public static boolean saveFile(String filePath, String fileName, boolean append, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            if(!filePath.endsWith("/")){
                filePath = filePath + "/";
            }
            File path = new File(filePath);
            if (!path.exists()) {
                if(!path.mkdirs()){
                    return false;
                }
            }
            File file = new File(filePath + fileName);
            if (!file.exists()) {
                if(!file.createNewFile()) {
                    return false;
                }
            }
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(buffer);
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Save buffer to file
     * @param filePath
     *         the file path with file name
     * @param append
     *         append
     * @param buffer
     *         buffer
     * @return boolean
     *
     */
    public static boolean saveFile(String filePath, boolean append, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            String fileDirs = filePath.substring(0, filePath.lastIndexOf('/'));
            File path = new File(fileDirs);
            if (!path.exists()) {
                if(!path.mkdirs()){
                    return false;
                }
            }
            File file = new File(filePath);
            if (!file.exists()) {
                if(!file.createNewFile()) {
                    return false;
                }
            }
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(buffer);
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @return byte[]
     *
     */
    public static byte[] readFile(String filePath, String fileName) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        try {
            if(!filePath.endsWith("/")){
                filePath = filePath + "/";
            }
            File file = new File(filePath + fileName);
            byte[] buffer = new byte[((Long)file.length()).intValue()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return buffer;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path with file name
     * @return byte[]
     *
     */
    public static byte[] readFile(String filePath) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        try {
            File file = new File(filePath);
            byte[] buffer = new byte[((Long)file.length()).intValue()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return buffer;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @param buffer
     *         the buffer of the file
     * @return boolean
     *
     */
    public static boolean readFile(String filePath, String fileName, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            if(!filePath.endsWith("/")){
                filePath = filePath + "/";
            }
            File file = new File(filePath + fileName);
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Read file to buffer
     * @param filePath
     *         the file path with file name
     * @param buffer
     *         the buffer of the file
     * @return boolean
     *
     */
    public static boolean readFile(String filePath, byte[] buffer) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (buffer == null) {
            throw new NullPointerException("buffer null");
        }
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer);
            fis.close();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete file
     * @param filePath
     *         the file path without file name
     * @param fileName
     *         the file name
     * @return boolean
     *
     */
    public static boolean deleteFile(String filePath, String fileName) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName null");
        }
        try {
            if(!filePath.endsWith("/")){
                filePath = filePath + "/";
            }
            File file = new File(filePath + fileName);
            if (file.exists() && file.isFile()) {
                return file.delete();
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete to file
     * @param filePath
     *         the file path with file name
     * @return boolean
     *
     */
    public static boolean deleteFile(String filePath) {
        if (filePath == null) {
            throw new NullPointerException("filePath null");
        }
        try {
            File file = new File(filePath);
            if (file.exists() && file.isFile()) {
                return file.delete();
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Copy file
     * @param fromFilePath
     *         the file path(from) with file name
     * @param toFilePath
     *         the file path(to) with file name
     * @return boolean
     *
     */
    public static boolean copyFile(String fromFilePath, String toFilePath) {
        if (fromFilePath == null) {
            throw new NullPointerException("fromFilePath null");
        }
        if (toFilePath == null) {
            throw new NullPointerException("toFilePath null");
        }
        try {
            FileInputStream fisFrom = new FileInputStream(fromFilePath);
            FileOutputStream fosTo = new FileOutputStream(toFilePath);
            byte b[] = new byte[1024];
            int n;
            while ((n = fisFrom.read(b)) > 0) {
                fosTo.write(b, 0, n);
            }
            fisFrom.close();
            fosTo.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Rename file
     * @param oldFilePath
     *         the file path(old) with file name
     * @param newFilePath
     *         the file path(new) with file name
     * @return boolean
     *
     */
    public static boolean renameFile(String oldFilePath, String newFilePath) {
        if (oldFilePath == null) {
            throw new NullPointerException("oldFilePath null");
        }
        if (newFilePath == null) {
            throw new NullPointerException("newFilePath null");
        }
        try {
            File oldFile = new File(oldFilePath);
            File newFile = new File(newFilePath);
            return oldFile.renameTo(newFile);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get stack trace string
     * @param tr
     *         Throwable
     * @return String
     *
     */
    public static String getStackTraceString(Throwable tr) {
        if (tr == null) {
            return "";
        }
        // This is to reduce the amount of log spew that apps do in the non-error
        // condition of the network being unavailable.
        Throwable t = tr;
        while (t != null) {
            if (t instanceof UnknownHostException) {
                return "";
            }
            t = t.getCause();
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    public static void showToast(final String msg){
        if(mContext != null) {
            showToast(mContext, null, msg);
        }
    }

    public static void showToast(final Context context, final String msg){
        if(context != null) {
            showToast(context, null, msg);
        }
    }

    public interface TstCfg {
        void tstCfg(Toast toast);
    }

    private static HandlerThread        mThreadToast    = null;
    private static Handler              mHandlerToast   = null;
    private static final boolean        mIsNewTrdToast  = true;

    public static void showToast(final Context context, final TstCfg config, final String msg) {
        class MsgObj{
            Context context;
            TstCfg config;
            String msg;
        }
        if(mIsNewTrdToast) {
            if ((mThreadToast == null) || (mHandlerToast == null)) {
                mThreadToast = new HandlerThread("mThreadToast");
                mThreadToast.start();
                mHandlerToast = new Handler(mThreadToast.getLooper()) {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        MsgObj msgObj = (MsgObj)msg.obj;
                        showToast(msgObj.context, msgObj.config, (CharSequence)msgObj.msg);
                    }
                };
            }
            Message message = Message.obtain();
            MsgObj msgObj = new MsgObj();
            msgObj.context = context;
            msgObj.config = config;
            msgObj.msg = msg;
            message.obj = msgObj;
            mHandlerToast.sendMessage(message);
        } else {
            showToast(context, config, (CharSequence)msg);
        }
    }

    private static void showToast(final Context context,  final TstCfg config, final CharSequence text){
        if(context == null){
            return;
        }
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        if(toast == null){
            return;
        }
        if(config != null) {
            config.tstCfg(toast);
        }
        toast.show();
    }

    /************************************************************************
     * Comm utils define
     ***********************************************************************/
    /**
     * Convert byte to hex string.
     * @param src
     *         byte data
     * @return
     *  hex string
     */
    public static String byteToHexString(byte src) {
        int v = src & 0xFF;
        String hz = "";
        String hv = Integer.toHexString(v);
        if (hv.length() < 2) {
            hz = "0";
        }
        hv = hz + hv;
        return hv;
    }

    /**
     * Convert byte[] to hex string.
     * @param src
     *         byte[] data
     * @return
     *  hex string
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * Parse byte[] to hex string.
     * @param src
     *         byte[] data
     * @return
     *  hex string
     */
    public static String parseBytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            stringBuilder.append("0x");
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
            if(i != src.length-1) {
                stringBuilder.append(" ");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Convert hex string to byte
     * @param hexString
     *         the hex string
     * @return byte
     */
    public static byte hexStringToByte(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return -1;
        }
        byte d = (byte) 0x00;
        int length = hexString.length();
        if(length == 1){
            hexString = "0" + hexString;
        }
        if(length > 2){
            hexString = hexString.substring(length-2, length);
        }
        d = (byte)(charToByte(hexString.charAt(0)) << 4 | charToByte(hexString.charAt(1)));
        return d;
    }

    /**
     * Convert hex string to byte[]
     * @param hexString
     *         the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Parse hex string to byte[]
     * @param hexString
     *         the hex string
     * @return byte[]
     */
    public static byte[] parseHexStringToBytes(String hexString) {
        boolean flag = false;
        int length = 0;
        int i = 0, j = 0;
        int start = 0, end = 0;
        byte[] buffer1 = null, buffer2 = null;
        length = hexString.length();
        buffer1 = new byte[length/2+1];
        for(i = 0; i < length; i++) {
            char ch = hexString.charAt(i);
            if(flag == false) {
                if((ch ==' ') || (ch =='\n') || (ch =='\r') || (ch =='\t')) {
                    flag = true;
                    end = i;
                    if(start<end) {
                        buffer1[j++] = hexStringToByte(hexString.substring(start, end));
                    }
                }
            }
            if(flag == true) {
                if(!((ch ==' ') || (ch =='\n') || (ch =='\r') || (ch =='\t'))) {
                    flag = false;
                    start = i;
                }
            }
            if(i == length-1) {
                if(!((ch ==' ') || (ch =='\n') || (ch =='\r') || (ch =='\t'))) {
                    flag = true;
                    end = length;
                    if(start<end) {
                        buffer1[j++] = hexStringToByte(hexString.substring(start, end));
                    }
                }
            }
        }
        length = j--;
        buffer2 = new byte[length];
        for(i = 0; i < length; i++) {
            buffer2[i] = buffer1[i];
        }
        return buffer2;
    }

    /**
     * Convert char to byte
     * @param c
     *         char
     * @return byte
     *
     */
     public static byte charToByte(char c) {
         byte B = (byte) "0123456789ABCDEF".indexOf(c);
         if(B != -1) {
             return B;
         }
         return (byte) "0123456789abcdef".indexOf(c);
    }

    /**
     * Checks whether the preferences contains a remember
     * @param context
     *         Context
     * @param name
     *         String
     * @param key
     *         String
     * @return boolean
     *
     */
    public static boolean sharedContains(Context context, String name, String key) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.contains(key);
        }
        return false;
    }

    /**
     * Mark in the editor that a preference value should be removed
     * @param context
     *         Context
     * @param name
     *         String
     * @param key
     *         String
     */
    public static void sharedRemove(Context context, String name, String key) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(key);
            editor.commit();
        }
        return;
    }

    /**
     * Mark in the editor to remove all values from the preferences
     * @param context
     *         Context
     * @param name
     *         String
     */
    public static void sharedClear(Context context, String name) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
        }
        return;
    }

    /**
     * SharedPreferences getXXXX functions
     */
    public static Map<String, ?> sharedGetAll(Context context, String name) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getAll();
        }
        return null;
    }

    public static String sharedGetString(Context context, String name, String key, String defVal) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getString(key, defVal);
        }
        return null;
    }

    public static Set<String> sharedGetStringSet(Context context, String name, String key, Set<String> defVal) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getStringSet(key, defVal);
        }
        return null;
    }

    public static int sharedGetInt(Context context, String name, String key, int defVal) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getInt(key, defVal);
        }
        return 0;
    }

    public static long sharedGetLong(Context context, String name, String key, long defVal) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getLong(key, defVal);
        }
        return 0;
    }

    public static float sharedGetFloat(Context context, String name, String key, float defVal) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getFloat(key, defVal);
        }
        return 0;
    }

    public static Boolean sharedGetBoolean(Context context, String name, String key, Boolean defVal) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            return sharedPreferences.getBoolean(key, defVal);
        }
        return false;
    }

    /**
     * SharedPreferences putXXXX functions
     */
    public static void sharedPutString(Context context, String name, String key, String value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
        }
        return;
    }

    public static void sharedPutStringSet(Context context, String name, String key, Set<String> value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putStringSet(key, value);
            editor.commit();
        }
        return;
    }

    public static void sharedPutInt(Context context, String name, String key, int value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(key, value);
            editor.commit();
        }
        return;
    }

    public static void sharedPutLong(Context context, String name, String key, long value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(key, value);
            editor.commit();
        }
        return;
    }

    public static void sharedPutFloat(Context context, String name, String key, float value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putFloat(key, value);
            editor.commit();
        }
        return;
    }

    public static void sharedPutBoolean(Context context, String name, String key, Boolean value) {
        if(context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(key, value);
            editor.commit();
        }
        return;
    }
}