package com.mine.libs.comm;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Debug {

    private static Context              mContext            = null;

    /************************************************************************
     * Constants define
     ***********************************************************************/
    private static String               ALL_TAG             = "Unknown";

    private static final boolean        ALL_SWT             = true;

    private static final boolean        LOG_SWT             = true;

    private static final boolean        FLE_SWT             = false;

    private static final boolean        EXT_SWT             = true;

    /************************************************************************
     * Functions define
     ***********************************************************************/
    static {
    }

    public static void setContext(Context context) {
        if(context != null) {
            mContext = context.getApplicationContext();
        }
    }

    public static void setTag(String tag) {
        ALL_TAG = tag;
    }

    public static String getTag() {
        return ALL_TAG;
    }

    public static void v(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.v(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("V", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void v(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.v(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("V", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void d(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.d(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("D", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void d(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.d(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("D", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void i(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.i(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("I", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void i(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.i(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("I", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void w(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.w(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("W", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void w(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.w(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("W", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }
    
    public static void e(String TAG, String msg){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.e(ALL_TAG, "["+TAG+"] " + msg);
            }
            if(FLE_SWT) {
                LogxF("E", ALL_TAG, "["+TAG+"] " + msg);
            }
        }
    }
    
    public static void e(String TAG, String msg, Throwable tr){
        if(ALL_SWT) {
            if(LOG_SWT) {
                Log.e(ALL_TAG, "["+TAG+"] " + msg, tr);
            }
            if(FLE_SWT) {
                LogxF("E", ALL_TAG, "["+TAG+"] " + msg, tr);
            }
        }
    }

    /**
     * Get FILE, FUNC, LINE and CLASS.
     * @return String or int
     *         result of FILE, FUNC, LINE and CLASS
     */
    private static final int stackdeep = 1;

    public static String FILE() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getFileName();
            }
        }
        return "";
    }

    public static String FUNC() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getMethodName();
            }
        }
        return "";
    }

    public static int LINE() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getLineNumber();
            }
        }
        return 0;
    }

    public static String CLASS() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                return new Throwable().getStackTrace()[stackdeep].getClassName();
            }
        }
        return "";
    }

    public static String CLASSPKG() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                String string = new Throwable().getStackTrace()[stackdeep].getClassName();
                string = string.substring(0, string.lastIndexOf('.'));
                return string;
            }
        }
        return "";
    }

    public static String CLASSSMP() {
        if(ALL_SWT) {
            if(EXT_SWT) {
                String string = new Throwable().getStackTrace()[stackdeep].getClassName();
                string = string.substring(string.lastIndexOf('.') + 1);
                return string;
            }
        }
        return "";
    }

    public static void LogxF(String lvl, String tag, String msg){
        saveLogfile(lvl + "/" + tag + ": " + msg);
    }

    public static void LogxF(String lvl, String tag, String msg, Throwable tr){
        saveLogfile(lvl + "/" + tag + ": " + msg + "\n" + getStackTraceString(tr));
    }

    private static String getStackTraceString(Throwable tr) {
        if (tr == null) {
            return "";
        }
        // This is to reduce the amount of log spew that apps do in the non-error
        // condition of the network being unavailable.
        Throwable t = tr;
        while (t != null) {
            if (t instanceof UnknownHostException) {
                return "";
            }
            t = t.getCause();
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    private static HandlerThread        mThreadFile     = null;
    private static Handler              mHandlerFile    = null;
    private static final boolean        mIsNewTrdFile   = true;

    private static void saveLogfile(String msg) {
        if(mIsNewTrdFile) {
            if ((mThreadFile == null) || (mHandlerFile == null)) {
                mThreadFile = new HandlerThread("mThreadFile");
                mThreadFile.start();
                mHandlerFile = new Handler(mThreadFile.getLooper()) {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        saveFile(msg.obj.toString());
                    }
                };
            }
            Message message = Message.obtain();
            message.obj = msg;
            mHandlerFile.sendMessage(message);
        }else {
            saveFile(msg);
        }
    }

    private static boolean saveFile(String data) {
        String appName = Comm.getApp();
        String filePath = "/z" + appName + "/Log/";
        String fileName = appName + ".txt";
        SimpleDateFormat simpleDateFormat = null;
        String time = null;
        String prefix = null;
        String contents = null;
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        time = simpleDateFormat.format(new Date());
        prefix = "log-" + "-"  + time + "-";
        simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
        time = simpleDateFormat.format(new Date());
        contents = time + '\t' + data + '\n';
        String storage = Comm.getStorage();
        if(storage != null) {
            return Comm.saveFile(storage + filePath, prefix + fileName, true, contents.getBytes());
        } else {
            return false;
        }
    }
}